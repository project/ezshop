EZShop
======
:firstname: CS�CSY L�szl�
:email: boobaa@kybest.hu
:date: K�sz�tette a KYbest-IT Kft. - Felhaszn�lhat� a mell�kelt GPLv2 szerint.
:revision: 1.0 - 2008. okt�ber 6.
:lang: hu

== Contents

== Le�r�s

A modul lehet�v� teszi tartalmak kos�rba helyez�s�t, a kos�r tartalm�nak
list�z�s�t, m�dos�t�s�t, valamint a megrendel�s emailben t�rt�n� elk�ld�s�t. A
kos�r felhaszn�l�hoz kapcsol�dik, �gy csak regisztr�lt �s bel�pett felhaszn�l�k
vehetik haszn�t - egy�b esetben a _Kos�rba_ link hat�s�ra csak egy felsz�l�t�st
kapunk a regisztr�ci�ra �s bel�p�sre. Egy felhaszn�l�nak csak egy kosara van,
mely a megrendel�s elk�ld�sekor �r�t�sre ker�l.

== Telep�t�s

A szok�sos m�don: a +sites/all/modules/+ k�nyvt�rba kicsomagolva.

== Be�ll�t�s

Az *Adminisztr�ci�* -> *Webhely be�ll�t�sa* -> *EZShop* oldalon ki kell
jel�lni, mely tartalomt�pusokhoz szeretn�nk webshopot. Meg lehet adni tov�bb�:

* a _Kos�rba_ link s�ly�t (mely a be�ll�tott t�pus� tartalmak oldal�n jelenik
  meg; alap�rtelmez�sben 99-es s�llyal, azaz val�sz�n�leg legalul);

* a megrendel�st milyen email-c�mre k�ldje el a rendszer (alap�rtelmez�sben
  �res, azaz a Drupal alapbe�ll�t�s�t haszn�lja);

* a kik�ld�tt email l�bl�c�ben megjelen� sz�veget (alap�rtelmez�sben �res).

== Megjelen�s

A modul a tartalom oldalra, valamint a list�kba k�pes _Kos�rba_ linket
elhelyezni.

== Tev�kenys�gek

=== Kos�rba t�tel

A kiv�laszott tartalomb�l egy darabot kos�rba tesz. Ha m�r volt a kos�rban ilyen
tartalom, akkor n�veli a darabsz�mot.

=== Kos�r megtekint�se

Felsorolja a bel�pett felhaszn�l� kosar�nak tartalm�t, az al�bbiak szerint:

* Mennyis�g: sz�vegmez�, a tartalom kos�rban lev� darabsz�m�val, mely lehet�v�
  teszi az adott tartalom kos�rban lev� mennyis�g�nek megv�ltoztat�s�t.
  Figyelem: v�ltoztat�s ut�n a kosarat a _Friss�t�s_ gombbal aktualiz�lni kell!
* C�m: a tartalom c�me linkk�nt, mely a tartalom oldal�ra vezet.
* Elt�vol�t�s link: az adott tartalmat teljesen elt�vol�tja a kos�rb�l.

A lista alatti _Friss�t�s_ gombbal lehet a kos�r tartalm�t friss�teni (pl. a
mennyis�g megv�ltoztat�sa ut�n), a _Megrendel�s_ gombbal pedig a be�ll�tott
c�mre elk�ldeni a megrendel�st.

=== Megrendel�s

A be�ll�tott email-c�mre a modul elk�ldi a kos�r tartalm�t, majd �r�ti azt. A
lev�l form�tuma (minta):

----
Mennyis�g -- C�m
1 -- Els� tartalom
3 -- M�sodik tartalom
----

Amennyiben a felhaszn�l� profillal is rendelkezik, �gy a kit�lt�tt profilmez�ket
is csatolja a modul (pl. _Teljes n�v_, _Sz�ll�t�si c�m_, stb.).

== Views t�mogat�s

A modul rendelkezik Views t�mogat�ssal, a k�vetkez�k szerint.

* Mez�k:
 - _EZShop: Kos�rba_ link;
 - _EZShop: Kiv�tel a kos�rb�l_ link;
 - _EZShop: Felhaszn�l�i azonos�t�_ (kihez tartozik az adott, kos�rban lev� tartalom);
 - _EZShop: T�telek sz�ma_ (az adott tartalomb�l mennyi van a kos�rban).
* Sz�r�k:
 - _EZShop: A tulajdonos a jelenlegi felhaszn�l�_ (igen/nem);
 - _EZShop: T�telek sz�ma_ (kisebb/nagyobb).

// vim: set tw=80:
