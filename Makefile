all:
	iconv -f latin2 -t utf8 README-hu.txt >README-hu-u.txt
	sed -i 's/^== Contents//' README-hu-u.txt
	sed -i 's/^:lang: hu//' README-hu-u.txt
	#a2x -v -f pdf README-hu-u.txt
	#a2x -v -f pdf --dblatex-opts="-P latex.output.revhistory=0 -P doc.publisher.show=0" README-hu-u.txt
	a2x -v -f pdf --dblatex-opts="-P latex.output.revhistory=0 -P doc.toc.show=0 -P doc.publisher.show=0" README-hu-u.txt

fop:
	sed 's/^== Contents//' <README-hu.txt >README-hu-u.txt
	asciidoc --doctype=article -b docbook -a encoding=latin2 README-hu-u.txt
	xsltproc titlepage-hacks-fop.xsl README-hu-u.xml >README-hu.fo
	fop -c /usr/share/fop/dejavu-ttf.xml -fo README-hu.fo -pdf README-hu.pdf

latex:
	iconv -f latin2 -t utf8 README-hu.txt >README-hu-u.txt
	asciidoc --unsafe --backend=latex -a toc README-hu-u.txt
	iconv -f utf8 -t latin2 README-hu-u.tex >README-hu.tex
	dos2unix README-hu.tex
	sed -i 's/^\(.usepackage{type1ec}\)/%\1/' README-hu.tex
	sed -i 's/\(.*\[\)english\(\].*\)/\1magyar\2/' README-hu.tex
	sed -i 's/^\(.usepackage{ucs}\)/%\1/' README-hu.tex
	sed -i 's/^\(.usepackage\[\)utf8x\(\]{inputenc}\)/\1latin2\2/' README-hu.tex
	sed -i 's/{Revision:}/{Verziószám:}/' README-hu.tex
	pdflatex README-hu.tex
	pdflatex README-hu.tex
	pdflatex README-hu.tex


clean:
	rm -f README-hu.{aux,idx,log,out,pdf,toc,tex,fo} README-hu-u.{txt,tex,xml,pdf}
